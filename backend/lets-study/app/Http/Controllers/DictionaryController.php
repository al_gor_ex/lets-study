<?php

namespace App\Http\Controllers;

use App\Http\Dto\Requests\DictionaryForm;
use App\Interfaces\IDictionaryService;
use App\Validators\DictionaryValidator;
use Illuminate\Http\Request;

class DictionaryController extends Controller
{
    public function __construct(
        private IDictionaryService $dictionaryService
    )
    {
    }

    public function store(Request $request)
    {
        $request->validate(DictionaryValidator::getRules());
        $this->dictionaryService->createDictionary(
            new DictionaryForm(
                $request->file('dictFile'),
                $request->file('photosArchive')
            )
        );
        return response(status: 201);
    }

    public function show(int $id)
    {
        return response()->json(
            data: $this->dictionaryService->getDictionary($id),
            options: JSON_UNESCAPED_SLASHES
        );
    }

    public function select()
    {
        return response()->json(
            $this->dictionaryService->getDictionariesSelectList()
        );
    }
}
