<?php

namespace App\Http\Controllers;

use App\Interfaces\IPairService;
use Illuminate\Http\Request;

class PairController extends Controller
{
    public function __construct(
        private IPairService $pairService
    )
    {
    }

    public function index(Request $request)
    {
        return response()->json(
            $this->pairService->getPairsList($request->query('search'))
        );
    }
}
