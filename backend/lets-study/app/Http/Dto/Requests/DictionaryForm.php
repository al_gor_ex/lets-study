<?php


namespace App\Http\Dto\Requests;


use Illuminate\Http\UploadedFile;

class DictionaryForm
{
    public function __construct(
        public UploadedFile $dictFile,
        public UploadedFile $photosArchive
    )
    {
    }
}
