<?php


namespace App\Http\Dto\Responses;


class PairModel
{
    public function __construct(
        public string $word,
        public string $translation
    )
    {
    }
}
