<?php


namespace App\Http\Dto\Responses;


class DictionaryResponse
{
    public string $name;
    public int $pairsCount;
    /**@var DictionaryPairModel[]*/
    public array $pairs = [];
}
