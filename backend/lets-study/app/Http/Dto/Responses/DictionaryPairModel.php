<?php


namespace App\Http\Dto\Responses;


class DictionaryPairModel
{
    public function __construct(
        public string $word,
        public string $translation,
        public ?string $photoUrl = null
    )
    {
    }
}
