<?php


namespace App\Http\Dto\Responses;


class PairsListResponse
{
    /**@var PairModel[] $pairs*/
    public array $pairs = [];
}
