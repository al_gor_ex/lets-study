<?php


namespace App\Http\Dto\Responses;


class DictionarySelectResponse
{
    public function __construct(
        public int $id,
        public string $name
    )
    {
    }
}
