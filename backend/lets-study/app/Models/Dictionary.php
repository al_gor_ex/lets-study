<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $name
 * @property Pair[] $pairs
 */
class Dictionary extends Model
{
    use HasFactory;

    public $timestamps = false;

    public function pairs()
    {
        return $this->hasMany(Pair::class);
    }
}
