<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $word
 * @property string $translation
 * @property string|null $photo_path
 * @property int $dictionary_id
 * @property Dictionary $dictionary
 */
class Pair extends Model
{
    use HasFactory;

    public $timestamps = false;

    public function dictionary()
    {
        return $this->belongsTo(Dictionary::class);
    }
}
