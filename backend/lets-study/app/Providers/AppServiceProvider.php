<?php

namespace App\Providers;

use App\Interfaces\IDictionaryService;
use App\Interfaces\IPairService;
use App\Services\DictionaryService;
use App\Services\PairService;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    public $bindings = [
        IDictionaryService::class => DictionaryService::class,
        IPairService::class => PairService::class
    ];

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
