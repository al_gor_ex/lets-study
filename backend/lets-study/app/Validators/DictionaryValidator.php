<?php


namespace App\Validators;


class DictionaryValidator
{
    public static function getRules(): array
    {
        return [
            'dictFile' => 'required|bail|file|mimes:csv,txt|max:5120',
            'photosArchive' => 'required|file|mimes:zip|max:102400'
        ];
    }
}
