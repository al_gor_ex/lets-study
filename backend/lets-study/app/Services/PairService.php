<?php


namespace App\Services;


use App\Http\Dto\Responses\PairModel;
use App\Http\Dto\Responses\PairsListResponse;
use App\Interfaces\IPairService;
use App\Models\Pair;
use Illuminate\Support\Collection;

class PairService implements IPairService
{

    public function getPairsList(?string $search = null): PairsListResponse
    {
        /**@var Pair[]|Collection $pairs */
        $pairs = Pair::query()
            ->orderBy('word')
            ->get();
        if (isset($search)) {
            $search = str($search)
                ->lower()
                ->toString();
            $pairs = $pairs->filter(
                fn(Pair $p) => str($p->word . $p->translation)->lower()->contains($search)
            );
        }
        $result = new PairsListResponse();
        foreach ($pairs as $pair) {
            $result->pairs[] = new PairModel($pair->word, $pair->translation);
        }
        return $result;
    }
}
