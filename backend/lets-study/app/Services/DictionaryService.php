<?php


namespace App\Services;


use App\Exceptions\PairPhotoNotFoundException;
use App\Exceptions\WrongDictionaryFormatException;
use App\Http\Dto\Requests\DictionaryForm;
use App\Http\Dto\Responses\DictionaryPairModel;
use App\Http\Dto\Responses\DictionaryResponse;
use App\Http\Dto\Responses\DictionarySelectResponse;
use App\Interfaces\IDictionaryService;
use App\Models\Dictionary;
use App\Models\Pair;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;
use League\Csv\Reader;
use PhpZip\ZipFile;

class DictionaryService implements IDictionaryService
{

    public function createDictionary(DictionaryForm $form): void
    {
        $dictName = $form->dictFile->getClientOriginalName();
        $folderName = Str::random();
        $dictFilePath = $form->dictFile->storeAs($folderName, $dictName);
        $photosArchivePath = $form->photosArchive->store($folderName);
        $csv = Reader::createFromPath(Storage::path($dictFilePath));
        $csv->setDelimiter(';');
        $csv->setHeaderOffset(0);
        $csvHeader = $csv->getHeader();
        if (array_diff(['ENG', 'RUS', 'Image'], $csvHeader) != []) {
            Storage::deleteDirectory($folderName);
            throw new WrongDictionaryFormatException($csvHeader);
        }
        $zip = new ZipFile();
        $zip->openFile(Storage::path($photosArchivePath));
        $zip->extractTo(storage_path("app/public/$folderName"));
        $zip->close();
        Storage::delete($photosArchivePath);
        /**@var Pair[] $pairs*/
        $pairs = [];
        $records = $csv->getRecords();
        foreach ($records as $offset => $record) {
            $pair = new Pair();
            $pair->word = $record['ENG'];
            $pair->translation = mb_convert_encoding($record['RUS'], "utf-8", "windows-1251");
            if (empty($record['Image'])) {
                $pairs[] = $pair;
            } else {
                $imagePath = "$folderName/${record['Image']}";
                if (Storage::exists($imagePath)) {
                    Image::make(Storage::path($imagePath))
                        ->heighten(config('images.pairPhotoHeight'))
                        ->save();
                    $pair->photo_path = $imagePath;
                    $pairs[] = $pair;
                } else {
                    Storage::deleteDirectory($folderName);
                    throw new PairPhotoNotFoundException($record['ENG'], $record['Image']);
                }
            }
        }
        Storage::delete($dictFilePath);
        $dictionary = new Dictionary();
        $dictionary->name = str($dictName)->explode('.')->first();
        $dictionary->save();
        foreach ($pairs as $pair) {
            $pair->dictionary_id = $dictionary->id;
            $pair->save();
        }
        Log::info('Created new dictionary', ['id' => $dictionary->id]);
    }

    public function getDictionary(int $id): DictionaryResponse
    {
        /**@var Dictionary $dict*/
        $dict = Dictionary::query()->findOrFail($id);
        $result = new DictionaryResponse();
        $result->name = $dict->name;
        $result->pairsCount = $dict->pairs()->count();
        foreach ($dict->pairs as $pair) {
            $result->pairs[] = new DictionaryPairModel(
                $pair->word,
                $pair->translation,
                (is_null($pair->photo_path)) ? null : Storage::url($pair->photo_path)
            );
        }
        return $result;
    }

    /** @return DictionarySelectResponse[] */
    public function getDictionariesSelectList(): array
    {
        /**@var Dictionary[] $dicts*/
        $dicts = Dictionary::query()
            ->orderBy('name')
            ->get();
        /**@var DictionarySelectResponse[] $result*/
        $result = [];
        foreach ($dicts as $dict) {
            $result[] = new DictionarySelectResponse($dict->id, $dict->name);
        }
        return $result;
    }
}
