<?php


namespace App\Exceptions;


use Exception;

class WrongDictionaryFormatException extends Exception
{
    public function __construct(
        private array $realHeader
    )
    {
        parent::__construct("Wrong dictionary's csv-file format", 100);
    }

    public function context()
    {
        return [
            'Uploaded file header' => $this->realHeader
        ];
    }

    public function render()
    {
        return response()->json([
            'code' => $this->code,
            'detail' => "Wrong dictionary's csv-file format"
        ], 422);
    }
}
