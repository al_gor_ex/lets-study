<?php


namespace App\Exceptions;


use Exception;

class PairPhotoNotFoundException extends Exception
{
    public function __construct(
        private string $word,
        private string $photoName
    )
    {
        parent::__construct("Words pair photo not found", 101);
    }

    public function context()
    {
        return [
            'word' => $this->word,
            'photoName' => $this->photoName
        ];
    }

    public function render()
    {
        return response()->json([
            'code' => $this->code,
            'detail' => "Cannot find photo {$this->photoName} for word {$this->word}"
        ], 422);
    }

}
