<?php


namespace App\Interfaces;


use App\Http\Dto\Responses\PairsListResponse;

interface IPairService
{
    public function getPairsList(?string $search = null): PairsListResponse;
}
