<?php


namespace App\Interfaces;


use App\Http\Dto\Requests\DictionaryForm;
use App\Http\Dto\Responses\DictionaryResponse;
use App\Http\Dto\Responses\DictionarySelectResponse;

interface IDictionaryService
{
    public function createDictionary(DictionaryForm $form): void;

    public function getDictionary(int $id): DictionaryResponse;

    /** @return DictionarySelectResponse[] */
    public function getDictionariesSelectList(): array;
}
