<?php

use App\Http\Controllers\DictionaryController;
use App\Http\Controllers\PairController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::prefix('/v1')->group(function() {
    Route::controller(DictionaryController::class)
        ->prefix('/dicts')
        ->group(function() {
            Route::post('/', 'store');
            Route::get('/select-list', 'select');
            Route::get('/{id}', 'show');
        });
    Route::controller(PairController::class)
        ->prefix('/pairs')
        ->group(function() {
            Route::get('/', 'index');
        });
});
