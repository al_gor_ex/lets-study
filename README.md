# Инструкция по установке

## Необходимое ПО
- PHP 8.1 (убедитесь, что установлены и включены расширение gd, а также расширения, указанные по [этой ссылке](https://laravel.com/docs/9.x/deployment#server-requirements))
- Composer
- MySQL Server
- Node.js v16 (+ npm)
- Git
- Любой текстовый редактор

## Команды для установки

1. Установите Angular

```
npm install -g @angular/cli
ng version
```

2. Установите Laravel

```
composer global require laravel/installer
```

3. Склонируйте/скачайте репозиторий в любую папку

``` bash
git clone https://gitlab.com/al_gor_ex/lets-study.git
```

4. Перейдите в папку бэкенд-проекта

``` bash
cd lets-study/backend/lets-study
```

5. Подключитесь к серверу MySQL и создайте БД (схему) с именем `lets_study`

6. В папке переименуйте файл `.env.example` в `.env` и откройте его в текстовом редакторе

7. Задайте параметры конфигурации (укажите Ваши значения) и сохраните файл

```ini
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=lets_study
DB_USERNAME=root
DB_PASSWORD=1234abcd
```

8. Установите пакеты для бэкенда

```
composer install --no-dev
```

9. Сгенерируйте ключ шифрования

```
php artisan key:generate
```

10. Создайте ссылку на хранилище файлов

```
php artisan storage:link
```

11. Выполните миграции БД

```
php artisan migrate
```

12. Запустите сервер

```
php artisan serve
```

13. Перейдите в папку фронтенд-проекта

``` bash
cd ../../frontend/lets-study
```

14. Установите пакеты для фронтенда

```
npm install
```

15. Если вы используете для бэкенд-проекта другой сервер (не Artisan), откройте в текстовом редакторе файл `environments/environment.ts` и исправьте значение константы `apiUrl` 

16. Запустите фронтенд-приложение и дождитесь окончания компиляции проекта

```
ng serve
```

17. Откройте браузер и перейдите по адресу `http://127.0.0.1:4200/` **(НЕ LOCALHOST !!)**. Должна отобразиться страница загрузки словаря.

18. В корневой папке репозитория лежит папка `dict-examples` с примером словаря и архива с изображениями для загрузки в систему.

ПРИМЕЧАНИЕ: при загрузке больших словарей и архивов с изображениями стоит увеличить значения настроек `memory_limit, upload_max_filesize и post_max_size` в файле `php.ini`