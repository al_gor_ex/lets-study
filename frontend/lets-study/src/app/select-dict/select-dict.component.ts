import { Component, OnInit } from '@angular/core';
import {DictionaryService} from "../services/dictionary.service";
import {DictionarySelectListResponse} from "../services/dto/responses/dictionary-select-list-response";
import {Router} from "@angular/router";

@Component({
  selector: 'app-select-dict',
  templateUrl: './select-dict.component.html',
  styleUrls: ['./select-dict.component.css']
})
export class SelectDictComponent implements OnInit {

  isLoading: boolean = true
  dicts: DictionarySelectListResponse[] = []
  selectedDictId: string = ""

  constructor(
    private dictService: DictionaryService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.dictService.getDictsSelectList().subscribe({
      next: response => {
        if (response.length == 0) {
          this.router.navigateByUrl('/upload-dict')
        }
        this.dicts = response
        this.selectedDictId = String(response[0].id)
        this.isLoading = false
      }
    })
  }

  onBtnStartClick(): void {
    this.router.navigateByUrl(`/learn-words/${this.selectedDictId}`)
  }

}
