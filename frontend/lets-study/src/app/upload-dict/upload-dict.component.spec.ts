import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UploadDictComponent } from './upload-dict.component';

describe('UploadDictComponent', () => {
  let component: UploadDictComponent;
  let fixture: ComponentFixture<UploadDictComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UploadDictComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UploadDictComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
