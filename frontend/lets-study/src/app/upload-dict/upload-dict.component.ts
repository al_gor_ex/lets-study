import {Component, OnInit, ViewChild} from '@angular/core';
import {DictionaryForm} from "../services/dto/dictionary-form";
import {DictionaryService} from "../services/dictionary.service";
import {SwalComponent} from "@sweetalert2/ngx-sweetalert2";
import {Router} from "@angular/router";

@Component({
  selector: 'app-upload-dict',
  templateUrl: './upload-dict.component.html',
  styleUrls: ['./upload-dict.component.css']
})
export class UploadDictComponent implements OnInit {

  isLoading: boolean = false;
  formErrors = {
    dictFile: false,
    photosArchive: false
  }
  @ViewChild('validationErrorSwal')
  validationErrorSwal: SwalComponent
  @ViewChild('serverValidationErrorSwal')
  serverValidationErrorSwal: SwalComponent
  @ViewChild('unknownErrorSwal')
  unknownErrorSwal: SwalComponent
  @ViewChild('successSwal')
  successSwal: SwalComponent


  constructor(
    private dictionaryService: DictionaryService,
    private router: Router
  ) {
  }

  ngOnInit(): void {
  }

  onSubmit(): void {
    this.isLoading = true
    this.validateForm()
    if (this.anyFormErrors()) {
      this.validationErrorSwal.fire()
      this.isLoading = false
      return
    }
    let postForm = new DictionaryForm()
    let form = document.forms[0]
    postForm.dictFile = form['dict-file'].files[0]
    postForm.photosArchive = form['photos-archive'].files[0]
    this.dictionaryService.uploadDict(postForm).subscribe({
      next: () => {
        this.successSwal.fire()
        this.router.navigateByUrl('/select-dict')
      },
      error: (error) => {
        this.isLoading = false
        console.log(error)
        switch (error.status) {
          case 422:
            this.serverValidationErrorSwal.fire()
            break
          default:
            this.unknownErrorSwal.fire()
            break
        }
      }
    })
  }

  validateForm(): void {
    this.validateDictFile();
    this.validatePhotosArchive();
  }

  anyFormErrors(): boolean {
    return Object.values(this.formErrors).includes(true)
  }

  validateDictFile(): void {
    let isValid = true
    let dictFileInput = document.getElementById('dict-file') as HTMLInputElement
    if (dictFileInput.files!.length == 0) {
      isValid = false
    } else {
      let file = dictFileInput.files!.item(0)!
      if (file.type !== 'text/csv' || file.size > 5 * 1_048_576) {
        isValid = false
      }
    }
    this.formErrors.dictFile = !isValid
  }

  validatePhotosArchive(): void {
    let isValid = true
    let archiveInput = document.getElementById('photos-archive') as HTMLInputElement
    if (archiveInput.files!.length == 0) {
      isValid = false
    } else {
      let file = archiveInput.files!.item(0)!
      if (!(['application/zip', 'application/x-zip-compressed'].includes(file.type)) ||
        file.size > 100 * 1_048_576) {
        isValid = false
      }
    }
    this.formErrors.photosArchive = !isValid
  }

}
