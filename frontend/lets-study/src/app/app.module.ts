import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { SelectDictComponent } from './select-dict/select-dict.component';
import { UploadDictComponent } from './upload-dict/upload-dict.component';
import { WordsListComponent } from './words-list/words-list.component';
import { LearnWordsComponent } from './learn-words/learn-words.component';
import {NgToggleModule} from "ng-toggle-button";
import {HttpClientModule} from "@angular/common/http";
import {SpinnersAngularModule} from "spinners-angular";
import {FormsModule} from "@angular/forms";
import {SweetAlert2Module} from "@sweetalert2/ngx-sweetalert2";

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    SelectDictComponent,
    UploadDictComponent,
    WordsListComponent,
    LearnWordsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    NgToggleModule.forRoot(),
    SpinnersAngularModule,
    SweetAlert2Module.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
