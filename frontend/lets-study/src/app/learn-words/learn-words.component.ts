import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {DictionaryService} from "../services/dictionary.service";
import {DictionaryResponse} from "../services/dto/responses/dictionary-response";

@Component({
  selector: 'app-learn-words',
  templateUrl: './learn-words.component.html',
  styleUrls: ['./learn-words.component.css']
})
export class LearnWordsComponent implements OnInit {

  languageSwitchSettings = {
    labels: {
      unchecked: "ENG→RUS",
      checked: "RUS→ENG"
    },
    colors: {
      "unchecked": "#e51c23",
      "checked": "#e51c23"
    },
    fontColors: {
      "checked": "#ffffff",
      "unchecked": "#ffffff"
    },
    switchColors: {
      "checked": "#ffffff",
      "unchecked": "#ffffff"
    },
    values: {
      "unchecked": 'ENG',
      "checked": 'RUS'
    }
  }
  isLoading: boolean = true
  dictionary: DictionaryResponse
  pairIndex: number = -1
  word: string
  translation: string
  isTranslationVisible: boolean = false
  doSwapLanguages: boolean = false
  private dictId: number

  constructor(
    public router: Router,
    private route: ActivatedRoute,
    private dictionaryService: DictionaryService
  ) {
  }

  ngOnInit(): void {
    this.dictId = Number(this.route.snapshot.paramMap.get('dictId'))
    this.loadDict()
  }

  getGenitiveCaseForNeuterWord(number: number, word: string): string {
    let n = number
    let suffix1 = "о"
    let suffix2 = "а"
    let last2Digits = n % 100
    let lastDigit = last2Digits % 10
    if (last2Digits >= 11 && last2Digits <= 14 || lastDigit >= 5 && lastDigit <= 9 || lastDigit == 0) {
      return word
    }
    if (lastDigit == 1) {
      return word + suffix1
    }
    if (lastDigit >= 2 && lastDigit <= 4) {
      return word + suffix2
    }
    throw new Error();
  }

  moveToNextPair(): void {
    this.isTranslationVisible = false
    this.pairIndex = (this.pairIndex >= this.dictionary.pairsCount - 1) ? 0 : (this.pairIndex + 1)
    let nextPair = this.dictionary.pairs[this.pairIndex]
    this.word = (this.doSwapLanguages) ? nextPair.translation : nextPair.word
    this.translation = (this.doSwapLanguages) ? nextPair.word : nextPair.translation
  }

  swapLanguages(): void {
    this.doSwapLanguages = !this.doSwapLanguages
    // Swap current pair's words
    let translationTemp = this.translation
    this.translation = this.word
    this.word = translationTemp
  }

  pronounce(text: string, lang: 'en-US'|'ru-RU'): void {
    let speech = new SpeechSynthesisUtterance()
    speech.text = text
    this.getVoicesList().then(voices => {
      let suitableVoices = voices.filter(v => v.lang === lang)
      if (suitableVoices.length == 0) {
        alert('Ошибка синтеза речи')
        return
      }
      speech.voice = suitableVoices[0]
      window.speechSynthesis.speak(speech)
    })
  }

  private loadDict(): void {
    this.dictionaryService.getDict(this.dictId).subscribe({
      next: value => {
        this.dictionary = value
        this.moveToNextPair()
        this.isLoading = false
      },
      error: err => {
        if (err.status == 404) {
          this.router.navigateByUrl('/select-dict')
        }
      }
    })
  }

  private getVoicesList(): Promise<SpeechSynthesisVoice[]> {
    return new Promise<SpeechSynthesisVoice[]>(
      function (resolve) {
        let synth = window.speechSynthesis
        let id = setInterval(() => {
          if (synth.getVoices().length !== 0) {
            resolve(synth.getVoices());
            clearInterval(id);
          }
        }, 10);
      }
    )
  }
}
