import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {SelectDictComponent} from "./select-dict/select-dict.component";
import {UploadDictComponent} from "./upload-dict/upload-dict.component";
import {WordsListComponent} from "./words-list/words-list.component";
import {LearnWordsComponent} from "./learn-words/learn-words.component";

const routes: Routes = [
  {
    path: 'select-dict',
    component: SelectDictComponent
  },
  {
    path: 'upload-dict',
    component: UploadDictComponent
  },
  {
    path: 'all-words',
    component: WordsListComponent
  },
  {
    path: 'learn-words/:dictId',
    component: LearnWordsComponent
  },
  {
    path: '',
    redirectTo: '/select-dict',
    pathMatch: 'full'
  },
  {
    path: '**',
    redirectTo: '/select-dict'
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
