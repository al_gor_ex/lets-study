import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {PairsListResponse} from "./dto/responses/pairs-list-response";
import {Observable} from "rxjs";
import {apiUrl} from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class PairService {

  private headers = {
    Accept: 'application/json'
  };

  constructor(private httpClient: HttpClient) {
  }

  getPairsList(search: string): Observable<PairsListResponse> {
    let url = new URL(`${apiUrl}/pairs`);
    if (search !== '') {
      url.searchParams.append('search', search);
    }
    return this.httpClient.get<PairsListResponse>(url.href, {headers: this.headers})
  }
}
