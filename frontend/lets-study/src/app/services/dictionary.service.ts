import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {DictionarySelectListResponse} from "./dto/responses/dictionary-select-list-response";
import {apiUrl} from "../../environments/environment";
import {DictionaryResponse} from "./dto/responses/dictionary-response";
import {DictionaryForm} from "./dto/dictionary-form";

@Injectable({
  providedIn: 'root'
})
export class DictionaryService {

  private headers = {
    Accept: 'application/json'
  };

  constructor(private httpClient: HttpClient) {
  }

  getDictsSelectList(): Observable<DictionarySelectListResponse[]> {
    return this.httpClient.get<DictionarySelectListResponse[]>(
      `${apiUrl}/dicts/select-list`,
      {headers: this.headers}
    );
  }

  getDict(id: number): Observable<DictionaryResponse> {
    return this.httpClient.get<DictionaryResponse>(
      `${apiUrl}/dicts/${id}`,
      {headers: this.headers}
    )
  }

  uploadDict(form: DictionaryForm): Observable<any> {
    let formData = new FormData()
    formData.append('dictFile', form.dictFile)
    formData.append('photosArchive', form.photosArchive)
    return this.httpClient.post<any>(
      `${apiUrl}/dicts`,
      formData,
      {headers: this.headers}
    )
  }
}
