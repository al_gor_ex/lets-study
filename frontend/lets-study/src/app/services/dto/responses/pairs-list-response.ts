export class PairsListResponse {
  pairs: {
    word: string
    translation: string
  }[]
}
