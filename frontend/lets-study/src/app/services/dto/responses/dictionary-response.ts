export class DictionaryResponse {
  name: string
  pairsCount: number
  pairs: {
    word: string
    translation: string
    photoUrl: string | null
  }[]
}
