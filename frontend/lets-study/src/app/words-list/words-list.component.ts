import {Component, OnInit} from '@angular/core';
import {PairsListResponse} from "../services/dto/responses/pairs-list-response";
import {PairService} from "../services/pair.service";
import {debounceTime, Subject} from "rxjs";

@Component({
  selector: 'app-words-list',
  templateUrl: './words-list.component.html',
  styleUrls: ['./words-list.component.css']
})
export class WordsListComponent implements OnInit {

  isLoading: boolean = true
  pairs: PairsListResponse
  searchInputText: string = ''
  searchInputEvent = new Subject<void>()
  search: string = ''

  constructor(private pairService: PairService) {
  }

  ngOnInit(): void {
    this.loadPairsList()
    this.searchInputEvent
      .pipe(debounceTime(500))
      .subscribe({
        next: () => {
          this.search = this.searchInputText
          this.loadPairsList()
        }
      })
  }

  private loadPairsList(): void {
    this.isLoading = true
    this.pairService.getPairsList(this.search).subscribe({
      next: response => {
        this.pairs = response
        this.isLoading = false
      }
    })
  }
}
